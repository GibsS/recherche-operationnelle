
% Définition des coûts
c1 = 5;
c2 = 5;
c3 = 10;
c4 = 5;
v = 20;

% Définition du step de computation
compute_step = 3;

% Défintion de la loi de probabilité des demandes
alpha = zeros(1,101);
for i=1:101,
    alpha(i) = (1+cos((2*pi*i/(101))-pi))/(101);
end

% Initialisation de la matrice des revenus
B = zeros(200);

% Remplissage de alpha ou Pi
alpha(end+1:size(B,1)+1) = 0;



% Calcul du revenu moyen hebdomadaire pour chaque couple (s,S)

% Initialisation du couple (s,S) testé
for S=1:compute_step:size(B,1),
    for s=1:compute_step:S,

        % Initialisation de la matrice de transition
        P = zeros(S+1);

        tmp = sum(alpha(S+1:end));
        for i=1:s,
            P(i,1) = tmp;
            for j=2:S+1,
                P(i,j) = alpha(S+1-j+1);
            end
        end

        for i=s+1:S+1,
            P(i,1) = sum(alpha(i:end));
            for j=2:S+1,
                if i>=j
                    P(i,j) = alpha(i-j+1);
                end
            end
        end

        % Fin de l'initialisation de P

        P = P - eye(S+1);
        P(:,end) = ones(S+1,1);
        Y = zeros(S+1,1);
        Y(end) = 1;
        % Calcul de la distribution limite Pi
        Pi = P'\Y;

        % Calcul du revenu moyen hebdomadaire
        tmp = 0;
        for i=1:S+1,
            % Calcul du revenu des ventes
            tmp = tmp + (i-1) * v * (Pi(i)*sum(alpha(i+1:end)) + alpha(i)*sum(Pi(i:S+1)));
            % Calcul du cout de stockage
            tmp = tmp - (i-1)*c1*Pi(i);
        end
        
        % Calcul du cout de pénurie
        for i=1:size(alpha,2)
            for j=1:min(size(Pi,1),i)
                tmp = tmp - (i-j) * c2 * Pi(j) * alpha(i);
            end
        end

        for i=1:s,
            % Calcul du prix d'une livraison
            tmp = tmp - c3*Pi(i);
            % Calcul du prix des commandes
            tmp = tmp - (S+1-i)*c4*Pi(i);
        end
        
        % Enregistrement du résultat
        B(s,S) = tmp;
    end
    % Affichage de l'indice de l'itération
    S
end

% Affichage des résultats
for i=1:size(B,1)
    for j=1:i
        B(i,j)=NaN;
    end
end
surf(B(1:compute_step:end,1:compute_step:end));

[~,ind] = max(B(:));
disp('Résultat :');
[s,S] = ind2sub(size(B),ind)
