function x = strategieS(numpart, tx, ty, gx, gy)
% strategie S* -- Strategie d'un joueur 
%
%  Usage
%    x = strategieS(numpart,tx,ty,gx,gy)
%
%  Inputs
%    tx     tableau des strategies jouees par le joueur x
%    ty     tableau des strategies jouees par le joueur y (l'adversaire)
%    gx     tableau des gains du joueur x
%    gy     tableau des gains du joueur y (l'adversaire)
%
%  Outputs
%    x      strategie elaboree par le joueur x
%
%  Description
%    Elaboration d'une strategie efficace dans le cadre de jeux iteres.


if numpart == 1
    x = 1.5; 
else 
    % Predire ty(numpart)
    m = numpart - 1;
    X = horzcat(ones(m,1), tx(1:m)');
    y = ty(1:m)';
    
     % Resolution approximative de l'equation en theta: X*theta=y
    theta = pinv(X'*X)*X'*y;
    
    % Retourner la meilleure reponse possible au coup ty(numpart) predi
    ypredi = [1 tx(m)]*theta;
    disp(theta);
    x = 2*(3-ypredi)/3;

end;