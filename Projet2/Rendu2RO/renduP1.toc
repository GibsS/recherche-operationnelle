\select@language {french}
\contentsline {part}{I\hspace {1em}Duopole de Cournot}{2}
\contentsline {chapter}{\numberline {1}Pr\'esentation du probl\`eme}{3}
\contentsline {chapter}{\numberline {2}\'Elements de r\'esolution}{4}
\contentsline {section}{\numberline {2.1}Lutter contre une strat\'egie coop\'erative}{4}
\contentsline {section}{\numberline {2.2}Lutter contre une strat\'egie affine}{4}
\contentsline {section}{\numberline {2.3}Lutter contre une strat\'egie non-coop\'erative}{4}
\contentsline {section}{\numberline {2.4}Lutter contre une strat\'egie de Stackelberg}{4}
\contentsline {chapter}{\numberline {3}Notre impl\'ementation de strat\'egie}{5}
\contentsline {section}{\numberline {3.1}Id\'ee g\'en\'erale}{5}
\contentsline {section}{\numberline {3.2}Apprentissage}{5}
\contentsline {section}{\numberline {3.3}R\'esultats}{5}
\contentsline {section}{\numberline {3.4}Approfondissement}{6}
\contentsline {chapter}{\numberline {4}Cha\IeC {\^\i }nes de Markov}{7}
\contentsline {section}{\numberline {4.1}Pr\'esentation du probl\`eme}{7}
\contentsline {section}{\numberline {4.2}\'Etude du syst\`eme}{7}
\contentsline {section}{\numberline {4.3}Calcul du revenu hebdomadaire moyen}{8}
\contentsline {section}{\numberline {4.4}Solution au probl\`eme}{8}
