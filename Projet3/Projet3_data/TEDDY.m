%--------------------------------------------------------------------------
%------------------------Planification online-----------------------------
%--------------------------------------------------------------------------
% Recherche du plus court chemin valide (verifiant les contraintes du probleme
close all;
clear all;
clc;

nbRobots = 3;
%% Creation de la carte numerique

CreateScene; % creation de la scene 'im'
imscene = im; % sauvegarde de l'image de la scene

%% Lecture des positions de depart et d'arrivee
for i=1:nbRobots
    disp ('*** Pointez le point de depart du robot : ');% Lecture point de depart du robot
    p = ginput(1);
    ideps(i) = round(p(2));
    jdeps(i) = round(p(1));
    
    disp ('*** Pointez le point d''arrivee du robot : ');% Lecture point d'arrivee du robot
    p = ginput(1);
    iarrs(i) = round(p(2));
    jarrs(i) = round(p(1));
end

%% Lecture des parametres du robot

diametre_robot = input('diametre des robot en nombre de pixels:','s'); % Lecture du diametre du robot en pixels
diametre_robot = str2double(diametre_robot);
r = round(diametre_robot/2); %rayon du robot

rho_min = input('Rayon de courbure minimum:','s'); % Lecture du rayon de courbure minimum
rho_min = str2double(rho_min);
global cseuil;
cseuil=1/rho_min; %contrainte de courbure

%% Squelettisation de l'espace libre des obstacles +  Raccordement des positions de depart et d'arrive
for i=1:nbRobots
    idep=ideps(i);
    jdep=jdeps(i);
    iarr=iarrs(i);
    jarr=jarrs(i);
    
    disp('Squelettisation en cours...');
    Squelettisation;
    
    %% Analyse du squelette etendu et conversion du squelette en un graphe
    
    ModelisationGraphe;
    
    %% Recherche du plus court chemin entre A et B dans G'
    
    PlusCourtChemin;
    figure('Name','Plus court chemin entre A et B dans le graphe'); %Affichage du chemin trouve
    
    imagesc(im)
    
    %% Lissage du chemin calcule par une courbe NURBS
    
    % Ordre de la courbe ; deg=order-1
    order=4;
    
    % Calcul des points de controle (coefs)
    coefs=zeros(2,niter); % tableau contenant les coordonnees cartesiennes des sommets
    %niter : nombre des sommets du chemin calcule (= size(chemin,2))
    
    for k=1:niter
        numsom = chemin(k);
        iint = double(icar(numsom));
        jint = double(jcar(numsom));
        coefs(:,k)=[jint,iint];
    end
    
    % Parameterisation du vecteur nodal
    knots=nrbKnots(niter,order); % Calcul du vecteur nodal
    % Nombre des points aa evaluer
    subdiv1=100;
    % Les ui
    ti = linspace(0.0,1.0,subdiv1);
    %--------------------------------------------------------------------------
    % utilisation d'un algorithme genetique pour la parameterisation des poids des points de controle
    %---------Parametres de l'AG-----------------------------------------------
    global max_weight;
    max_weight=10;% valeur max des poids
    PopulationSize=100; % taille de la population
    p_mut=1;% Probabilite de mutation
    modif_rate = 5;% pourcentage de croisement
    nb_iter=15; %nombre d'iterations
    %--------Execution de l'AG----------------------------------------------
    tic;
    disp('GAforNURBScurveFitting en cours d execution...');
    [Eval,W_Elu,indice_elu,sol,weightsInit,weights]= GAforNURBScurveFitting(im,coefs,knots,subdiv1,PopulationSize,max_weight,cseuil,r,modif_rate, nb_iter,p_mut);
    toc;
    
    %% Analyse des Resultas
    
    %W_Elu = solution retenue (vecteur poids)
    coefs1 = zeros(4,niter); % Passage en coordonnees homogenes
    coefs1(1,:) = coefs(1,:) .* W_Elu(1,:);
    coefs1(2,:) = coefs(2,:) .* W_Elu(1,:);
    coefs1(4,:) = W_Elu(:,:);
    
    nurbsf(i) = nrbmak2(coefs1,knots);   % Construction de la structure NURBS
    
    nbPrbCur=Eval(indice_elu).nb_CC; % nombre de points de la courbe qui ne respectent pas la contrainte de courbure
    nbPrbCol=Eval(indice_elu).nb_CO; % nombre de points de la courbe qui ne respectent pas la contrainte de la non-collision
    
end

%% TEDDY
dt = 0.1;
%% Initialisation
portee = 20;
for i=1:nbRobots
    i_curr(i) = ideps(i);
    j_curr(i) = jdeps(i);
    % Déplacement du robot i sur une NURBS à vitesse constante
    subd = 1000;
    pos(i) = nrbeval(nurbsf(3), linspace(0.0, 1.0, subd));
    %pos2(i) = pos(i)(1:2,:);
    s1(i) = arclength(pos(i)(1,:), pos(i)(2,:));
    v = 1; % vitesse constante = 1 px / itération
    s(i) = 0:v:s1(i);
    p(i) = 1:subd;
    pp(i) = pdearcl(p(i), pos(i),s(i),0,s1(i));
    % au bout d'un temps t (t=0,1,2,..,length(pp) le robot se situe à
    % (pos(i)(1,pp(i)(t), pos(i)(2,pp(i)(t))
end

%% Simulation
etat = 0; % 0 : sur la courbe normal; 1 : hors chemin
while(i_curr(1) ~= iarrs(1) && j_curr(1) ~= jarrs(1))
    % Simulation d'avancement à chaque instant t pour tous les robots
    for i=1:nbRobots
        
        % Vision robots
        for j=1:nbRobots
            xRobots = [xRobots, pos(1,round(pp(j)(t)))];
            yRobots = [yRobots, pos(2,round(pp(j)(t)))];
        end
        
        resultanteRobot = visionRobot(xRobots, yRobots, portee);
        resultanteObstacle = visionObstacle(xRobots,yRobots,obstacles);
        
        if(etat == 0)
            if(norm(resultanteRobot) > eps)
                % simulation dans le futur
                normeResultante = 1;
                t_aux = t;
                while(normeResultante <= eps)
                    t_aux = t_aux + 1;
                    for j=1:nbRobots
                        xRobots = [xRobots, pos(1,round(pp(j)(t_aux)))];
                        yRobots = [yRobots, pos(2,round(pp(j)(t_aux)))];
                    end
                    normeResultante = visionRobot(xRobots, yRobots, portee);
                end
                % point d'attraction
                x_attraction = pos(1)(1,pp(1)(t_aux));
                y_attraction = pos(1)(2,pp(1)(t_aux));
                etat = 1;
            end
        end
        if(etat == 1)
            if sqrt(((x_attraction - xRobots(1))^2 + (y_attraction - yRobots(1))^2) < eps
                etat = 0;
            end
        end
        
        if(etat == 0)
            % Avancement dans l'espace
            i_curr(1) = pos(1,round(pp(1)(t)));
            j_curr(1) = pos(2,round(pp(1)(t)));
        elseif(etat == 1)
            % Forces de répulsion -> déplacement
            % dues aux robots mobiles:
            i_curr(1) = i_curr(1) + norm(resultanteRobot(2)-resultanteRobot(1))*resultanteRobot(1);
            j_curr(1) = j_curr(1) + norm(resultanteRobot(2)-resultanteRobot(1))*resultanteRobot(2);
            % dues aux obstacles fixes:
            i_curr(1) = i_curr(1) + norm(resultanteObstacle(2)-resultanteObstacle(1))*resultanteObstacle(1);
            j_curr(1) = j_curr(1) + norm(resultanteObstacle(2)-resultanteObstacle(1))*resultanteObstacle(2);
        end
             
    end
    % Avancement dans le temps
    t = t + 1;
end

%%
figure;
colormap(jet);
im = imscene;
imagesc(im);hold on;

colormap(jet);
plot(coefs(1,:),coefs(2,:),'.r');%% les sommets de la courbe aa approximer (formant le polygone de controle)
hold on

colormap(jet);
[p1f,nurbs_corf] = nrbplot(nurbsf,subdiv1,'m');%% Affichage de la courbe NURBS
title('Solution Offline retenue')
hold on;
colormap(jet);
saveas(gcf,'trajectoiref.tif');

if sol  % si la solution est admissible
    [nurbsFinal] = TransformationImageRepere(nurbsf);
    [courburenurbsFinal, courbure_moynurbsFinal]= CalculCourbure(nurbsFinal,subdiv1);
    figure;
    plot(ti,courburenurbsFinal(1,:),'r');xlabel('ui');ylabel('curvature');title('Courbure de la trajectoire');hold on;
else
    % localisation des positions non acceptables si pas de solution
    trackRobot(Eval,nbPrbCur,nbPrbCol,indice_elu,sol,ti,cseuil,subdiv1,im,r);
    
    [nurbsFinal] = TransformationImageRepere(nurbsf);
    [courburenurbsFinal, courbure_moynurbsFinal]= CalculCourbure(nurbsFinal,subdiv1);
    figure;
    plot(ti,courburenurbsFinal(1,:),'r');xlabel('ui');ylabel('curvature');title('Courbure de la trajectoire');hold on;
    % Localisation des points critiques courbure
    [vect_CC]=DetectionCC(ti,courburenurbsFinal,cseuil,nbPrbCur);
end;

%%
%%%%%%%%Affichage des parametre input%%%%%%%%%%%%%%%%%%%
disp('******************Input parameters**********************');
disp(['Space size : N=',num2str(nlig),' M= ',num2str(ncol)]);
disp(['Start position : xi=',num2str(jdep),' yi= ',num2str(idep)]);
disp(['Target position : xf=',num2str(jarr),' yf= ',num2str(iarr)]);
disp(['Robot diameter : d=',num2str(diametre_robot)]);
disp(['Minimum radius of curvature : rho_min=',num2str(rho_min)]);

disp('********************************************************');

