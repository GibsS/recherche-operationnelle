function point=projection(position_point,obstacle)
%position_point est un vecteur colonne x,y
%obstacle un vecteur colonne a 4 lignes
if(position_point(2)<=obstacle(3))
    if(position_point(1)<=obstacle(1))
        point=[obstacle(1);obstacle(3)];
    else
        if(position_point(1)<=obstacle(2))
            point=[position_point(1);obstacle(3)];
        else
            point=[obstacle(2);obstacle(3)];
        end
    end
else
    if(position_point(2)>=obstacle(4))
        if(postion_point(1)<=obstacle(4))
            point=[obstacle(1);obstacle(4)];
        else
            if(position_point(1)<=obstacle(2))
                point=[position_point(1);obstacle(4)];
            else
                point=[obstacle(2);obstacle(4)];
            end
        end
    else
        if(position_point(1)<=obstacle(1))
            point=[obstacle(1);position_point(2)];
        else
            point=[obstacle(2);position_point(2)];
        end
    end
end