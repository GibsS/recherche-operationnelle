function collision = detectionCollision(trajectoire1abscisse, trajectoire1ordonnee, trajectoireObstacleAbscisse, trajectoireObstacleOrdonnee, sommeRayon, temps)

%collision vaut 1 si il n'y a pas collision
collision = 1

for i = 1:temps
    
    collision = collision * (sqrt((trajectoireObstacleAbscisse(i) - trajectoire1abscisse(i))^2 + (trajectoireObstacleAbscisse(i) - trajectoire1ordonnee(i)))
    if collision==0
        break;
    end
end