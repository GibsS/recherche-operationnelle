function [resultante] = visionRobot(xposition,yposition,portee)
kext = 2;
ws = 1;
%les points sont au meme temps
%les vecteurs contiennent la position du robot important en place 1
%dext = distances
distances = sqrt((ws*(xposition(2:end) - xposition(1)))^2 + (ws*(yposition(2:end) - yposition(1)))^2);
distances = distances(find(distances<portee));
xResultante = kext*sum(-distances.*(xposition(2:end) - xposition(1))./distances);
yResultante = kext*sum(-distances.*(yposition(2:end) - yposition(1))./distances);
resultante(1,1) = xResultante;
resultante(2,1) = yResultante;

