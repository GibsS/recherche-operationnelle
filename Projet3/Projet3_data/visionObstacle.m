function [resultante] = visionObstacle(xposition,yposition,obstacles)
%obstacles : matrice de 4 lignes(xmin,xmax,ymin,ymax) et une colonne par
%obstacle
kext = 2;
ws = 1;
x=xposition;
y=yposition;
%les points sont au meme temps
%les vecteurs contiennent la position du robot important en place 1
%dext = distances
for i=1:size(obstacles,2)
    projection_point=projection([x;y],obstacles(:,i));
    distance = sqrt((ws*(projection_point(1)-x))^2+(ws*(projection_point(2)-y))^2);
    
    xResultante = -kext*distance*(projection_point(1)-x);
    yResultante = -kext*distance*(projection_point(2)-y);
    
end
resultante(1,1) = xResultante;
resultante(2,1) = yResultante;

