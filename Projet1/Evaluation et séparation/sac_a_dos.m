% Ceci est un petit test de notre algorithme de résolution du problème du
% sac à dos :
A = [9 7 5 6];
b = 30;
c = [3 4 5 2];
OPT = 1;
seuil = solution_initiale(c, A, b);
borne_max = [2; 2; 2; 2]; % on accepte 2 de chaque élément
borne_min = [0; 0; 0; 0];
[x_eval1, x_int1] = test(OPT, c, A, b, borne_min, borne_max, seuil);

A = [9 8 6 5 4 1];
b = 10;
c = [20 16 11 9 7 1];
OPT = 1;
seuil = solution_initiale(c, A, b);
borne_max = [1; 1; 1; 1; 1; 1];
borne_min = [0; 0; 0; 0; 0; 0];
[x_eval2, x_int2] = test(OPT, c, A, b, borne_min, borne_max, seuil);