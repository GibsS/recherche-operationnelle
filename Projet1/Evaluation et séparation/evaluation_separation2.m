function [x,F] = evaluation_separation2(OPT, c, A, b, borne_min, borne_max, seuil)
%% Calcul une solution à un problème de programmmation linéaire en variable entière.
% Cette algorithme implémente l'algorithme d'évaluation et séparation : on
% se rapporte à un problème de minimisation et on construit progressivement
% des solutions potentielles.

%% Donnée en entrée
% OPT : 1 si on maximise, -1 si on minimise
% c : fonction d'évaluation
% A : matrice des contraintes
% b : vecteur des contraintes
% borne_min : vecteur des valeurs minimales de chaque composante
% borne_max : vecteur des valeurs maximales de chaque composante
% seuil : le poids de la solution de départ

%% Donnée retournée
% x : la solution 
% F : la valeur de la solution par rapport au problème 

%% Variable de l'algorithme
% c_min : la fonction d'évaluation modifiée pour se rapporter à un
% problème de minisation
% n : nombre de du problème
% seuil : valeur associée à la solution courante : une fois un vecteur
% entièrement construit, si il est de valeur inférieure au seuil, il
% remplace la solution optimale et change le seuil.
% seuil_change : variable temporaire utilisée pour savoir si la solution
% optimale a changé, voulant dire que certaines branches doivent être
% éliminées.
% poids_frontiere (liste) : la "valeur" ( valeur de la solution par résolution continue)
% des vecteurs courrament en train d'être construit.
% frontiere (liste) : la liste des vecteurs en train d'être construit.
% Concrètement, cette liste représente les feuilles de l'arbre de
% l'algorithme d'évaluation et séparation à chaque itération.

%% Etape 1 : Initialisation
c_min = - OPT * c;
n = size(c_min,2);

% seuil = solution_initiale(c_min, A, b);
seuil = -OPT * seuil;
seuil_change = false;

poids_frontiere = [0];
frontiere = {[]};

%% Etape 2 : Itérations jusqu'à ce qu'on est une solution
% durant la boucle, on fait évoluer poids_frontiere : la "valeur" de chaque
% vecteur partiel en y rajoutant des nouveaux poids et en supprimant celui
% du vecteur que l'on étudie. La solution est trouvée si on a trouvé toutes
% les composantes d'un vecteur et que ce dernier a le poid minimal. On trie
% les vecteurs à chaque fin de boucle donc frontiere{1} est le vecteur
% partiel de plus petit poid.
while size(frontiere{1},1) < n 
    %% Etape 2.1 : Initialisation de la boucle 
    % On récupère le vecteur avec la valeur optimale et on le supprime de
    % la liste 
    vecteur_courant = frontiere{1};
    taille_courante = size(vecteur_courant, 1) + 1;
    frontiere(1) = [];
    poids_frontiere(1) = [];
    % On produit ses fils
    fils = {};
    for i = borne_min(taille_courante):borne_max(taille_courante)
        fils{end+1} = [vecteur_courant; i];
    end

    %% Etape 2.2 : traitement des fils
    % l'évaluation du poids pour un vecteur "complet" (avec toutes ces 
    % composantes définies) diffère de celle d'un vecteur partiel d'où le
    % traitement séparé
    if taille_courante == n
        for i = 1:size(fils,2)
            poids = c_min * fils{i};
            if (poids <= seuil) && (all(A * fils{i} <= b))
                seuil = poids;
                seuil_change = true;
                poids_frontiere = [poids_frontiere poids];
                frontiere{end + 1} = fils{i};
            end 
        end
        
        if seuil_change
            I2 = setdiff(1:size(poids_frontiere,2), find(poids_frontiere<seuil));
            frontiere = frontiere(I2);
            poids_frontiere = poids_frontiere(I2);
            seuil_change = false;
        end
    else
        for i = 1: size(fils,2)
            if all(A(1,1:taille_courante) * fils{i} <= b)
                [~, poids] = linprog(c_min(1,taille_courante+1:n), A(:,taille_courante+1:n), b - A(:, 1:taille_courante)*fils{i},...
                    zeros(1,n-taille_courante), 0, borne_min(taille_courante+1:n), borne_max(taille_courante+1:n));
                poids = poids + c_min(1, 1:taille_courante) * fils{i};
                if poids <= seuil
                    poids_frontiere = [poids_frontiere poids];
                    frontiere{end + 1} = fils{i};
                end
            end
        end
    end
       
    %% Etape 2.3 : Trie du nouvelle ensemble de vecteur
    % on trie par rapport à la valeur de chacun en préparation à la
    % prochaine itération.
    [~,I] = sort(poids_frontiere);
    poids_frontiere = poids_frontiere(I);
    frontiere = frontiere(I);
    
end

F = -OPT * poids_frontiere(1);
x = frontiere{1};

end
