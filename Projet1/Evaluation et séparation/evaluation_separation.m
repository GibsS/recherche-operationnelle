function [x,F] = evaluation_separation(OPT, c, A, b)
%% Calcul une solution à un problème de programmmation linéaire en variable entière.
% Cette algorithme implémente l'algorithme d'évaluation et séparation : on
% se rapporte à un problème de minimisation et on construit progressivement
% des solutions potentiels.

%% Donnée en entrée
% OPT : 1 si on maximise, -1 si on minimise
% c : fonction d'évaluation
% A : matrice des contraintes
% b : vecteur des contraintes

%% Donnée retournée
% x : la solution 
% F : la valeur de la solution par rapport au problème 

%% Variable de l'algorithme
% c_min : la fonction d'évaluation modifié pour que se rapporter à un
% problème de minisation
% n : nombre de du problème
% seuil : valeur associé à la solution courante : une fois un vecteur
% entièrement construit, si il est de valeur inférieur au seuil, il
% remplace la solution optimal et change le seuil.
% seuil_change : variable temporaire utilisé pour savoir si la solution
% optimale à changé, voulant dire que certaines branches doivent être
% éliminés.
% poids_frontiere (liste) : la "valeur" ( valeur de la solution par résolution continue)
% des vecteurs courrament en train d'être construit.
% frontiere (liste) : la liste des vecteurs en train d'être construit.
% Concrètement, cette liste représente les feuilles de l'arbre de
% l'algorithme d'évaluation et séparation à chaque itération.

%% Etape 1 : Initialisation
c_min = - OPT * c;
n = size(c_min,2);

seuil = solution_initiale(c_min, A, b);
seuil_change = false;

poids_frontiere = [0];
frontiere = {[]};

%% Etape 2 : Itérations jusqu'à ce qu'on est une solution
% Invariant : la solution optimale 
while size(frontiere{1},1) < n 
    %% Etape 2.1 : Initialisation de la boucle 
    % On récupère le vecteur avec la valeur optimale et on le supprime de
    % la liste
    vecteur_courant = frontiere{1};
    frontiere(1) = [];
    poids_frontiere(1) = [];
    % On produit ses fils
    fils_gauche = [vecteur_courant; 0];
    fils_droit = [vecteur_courant; 1];

    %% Etape 2.2 : traitement des fils
    if size(fils_gauche,1) == n
        poids = c_min * fils_gauche;
        if (poids <= seuil) && (A * fils_gauche <= b)
            seuil = poids;
            seuil_change = true;
            poids_frontiere = [poids_frontiere poids];
            frontiere{end+1} = fils_gauche;
        end
        poids = c_min * fils_droit;
        if (poids <= seuil) && (A * fils_droit <= b)
            seuil = poids;
            seuil_change = true;
            poids_frontiere = [poids_frontiere poids];
            frontiere{end+1} = fils_droit;
        end
        
        if seuil_change
            I2 = setdiff(1:size(poids_frontiere,2), find(poids_frontiere<seuil));
            frontiere = frontiere(I2);
            poids_frontiere = poids_frontiere(I2);
            seuil_change = false;
        end
    else
        if A(1,1:size(fils_gauche,1)) * fils_gauche <= b;
            [~, poids] = linprog(c_min(1,size(fils_gauche,1)+1:n), A(:,size(fils_gauche,1)+1:n), b - A(:,1:size(fils_gauche,1))*fils_gauche,...
                zeros(1, n-size(fils_gauche,1)), 0, zeros(n-size(fils_gauche,1),1), ones(n-size(fils_gauche,1),1)); 
            poids = poids + c_min(1,1:size(fils_gauche,1)) * fils_gauche;
            if poids <= seuil
                poids_frontiere = [poids_frontiere poids];
                frontiere{end+1} = fils_gauche;
            end
        end
        if A(1,1:size(fils_droit,1)) * fils_droit <= b;
            [~, poids] = linprog(c_min(1,size(fils_gauche,1)+1:n), A(:,size(fils_droit,1)+1:n), b - A(:,1:size(fils_droit,1))*fils_droit,...
                zeros(1, n-size(fils_droit,1)), 0, zeros(n-size(fils_droit,1),1), ones(n-size(fils_droit,1),1));
            poids = poids + c_min(1,1:size(fils_droit,1)) * fils_droit;
            if poids <= seuil
                poids_frontiere = [poids_frontiere poids];
                frontiere{end+1} = fils_droit;
            end
        end
    end
       
    %% Etape 2.3 : Trie du nouvelle ensemble de vecteur
    % on trie par rapport à la valeur de chacun en préparation à la
    % prochaine itération.
    [~,I] = sort(poids_frontiere);
    poids_frontiere = poids_frontiere(I);
    frontiere = frontiere(I);
    
end

F = -OPT * poids_frontiere(1);
x = frontiere{1};

end
