function s = solution_initiale(c, A, b)
%% Une fonction qui calcule de manière naive une solution naive et non
% optimal au problème du sac à dos en rajoutant progressivement des
% éléments aux sac à dos
    n = size(c,2);
    valeur_poids = -c./A;
    [~,I] = sort(valeur_poids, 'descend');
    valeur_poids = valeur_poids(I);
    sol = zeros(n,1);
    poids_courant = 0;

    for i = 1:n 
        if poids_courant + A(I(i)) <= b
            sol(I(i)) = 1;
            poids_courant = poids_courant + A(I(i));
        end
    end
    
    s = c*sol;
end