function [x_eval, x_int] = test(OPT, c, A, b, borne_min, borne_max, seuil) 
%% Un petit programme de test avec evaluation separation et intlinprog 
% résolvant le même problème du sac à dos
[x_eval,F] = evaluation_separation2(OPT, c, A, b, borne_min, borne_max, seuil);

A_util = [A; eye(size(c,2)); -eye(size(c,2))];
b_util = [b; borne_max; borne_min];
x_int = intlinprog(-OPT * c, 1:size(c,2), A_util, b_util);