\select@language {french}
\contentsline {part}{I\hspace {1em}R\'esolution de probl\`emes de programmation lin\'eaire}{3}
\contentsline {chapter}{\numberline {1}Introduction}{4}
\contentsline {chapter}{\numberline {2}Emploi du temps}{5}
\contentsline {section}{\numberline {2.1}Introduction}{5}
\contentsline {section}{\numberline {2.2}Mod\'elisation}{5}
\contentsline {section}{\numberline {2.3}R\'esultats}{7}
\contentsline {subsection}{\numberline {2.3.1}Premier r\'esultat}{7}
\contentsline {subsection}{\numberline {2.3.2}Second r\'esultat}{8}
\contentsline {section}{\numberline {2.4}Probl\`eme avec notre solution}{9}
\contentsline {chapter}{\numberline {3}Sac \`a dos}{10}
\contentsline {section}{\numberline {3.1}Introduction}{10}
\contentsline {section}{\numberline {3.2}Mod\'elisation}{10}
\contentsline {section}{\numberline {3.3}M\'ethode de r\'esolution}{10}
\contentsline {section}{\numberline {3.4}R\'esultat}{10}
\contentsline {part}{II\hspace {1em}R\'esolution de probl\`emes de flot et de tension maximum}{13}
\contentsline {chapter}{\numberline {4}Repr\'esentation en m\'emoire d'un graphe}{14}
\contentsline {chapter}{\numberline {5}Probl\`eme de maximisation de flot}{15}
\contentsline {section}{\numberline {5.1}Description du probl\`eme}{15}
\contentsline {section}{\numberline {5.2}Notre impl\'ementation de l'algorithme de r\'esolution de Ford-Fulkerson }{15}
\contentsline {section}{\numberline {5.3}Application au probl\`eme des transports de ressources maritimes}{17}
\contentsline {subsection}{\numberline {5.3.1}Mod\'elisation du probl\`eme}{17}
\contentsline {subsection}{\numberline {5.3.2}R\'esultat}{17}
\contentsline {chapter}{\numberline {6}Probl\`eme de maximisation de tension}{18}
\contentsline {section}{\numberline {6.1}Description du probl\`eme}{18}
\contentsline {section}{\numberline {6.2}Notre impl\'ementation de l'algorithme de r\'esolution de Ford-Fulkerson }{19}
\contentsline {section}{\numberline {6.3}Application au probl\`eme d'ordonnancement}{19}
\contentsline {subsection}{\numberline {6.3.1}Mod\'elisation du probl\`eme}{19}
\contentsline {subsection}{\numberline {6.3.2}R\'esultat}{20}
\contentsline {part}{III\hspace {1em}Annexe}{22}
\contentsline {chapter}{\numberline {7}R\'epartition des t\^aches}{23}
\contentsline {chapter}{\numberline {8}Structure du code source}{24}
\contentsline {section}{\numberline {8.1}Simplexe}{24}
\contentsline {section}{\numberline {8.2}Emploi du temps}{24}
\contentsline {section}{\numberline {8.3}Evaluation et S\'eparation}{24}
\contentsline {section}{\numberline {8.4}Ford et Fulkerson pour les tensions}{24}
\contentsline {subsection}{\numberline {8.4.1}Application au probl\`eme maritime}{24}
\contentsline {subsection}{\numberline {8.4.2}Application au probl\`eme d'ordonnancement}{25}
