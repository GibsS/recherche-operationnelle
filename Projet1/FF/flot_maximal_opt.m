function [flot_max, flot] = flot_maximal_opt(nb_noeud, depart, fin, succ, capacite)

%% Implementation de l'algorithme Ford-Fulkerson en utilisant un parcours en largeur.
% Implementation d'Edmonds-karp de Ford-Fulkerson. L'utilisation d'un
% parcours en largeur ameliore la complexite de l'algorithme de
% maximisation du flot.

%% Donnees d'entree
% nb_noeud: le nombre de noeud du graphe
% depart: la source 
% fin: le puit 
% succ: containers.Map(int, list) qui contient les successeurs de chaque noeud, le
%       successeur de fin pris comme ?tant la liste vide.
% capacite: containers.Map(int, containers.Map(int,int)) qui contient les
%       capacites de chaque arrete du graphe.

%% Donnees de sortie
% flot_max: flot_maximal atteint dans le graphe
% flot: matrice ou flot(u,v) indique le flot dans l'arrete u->v

%% Initialisation
flot_est_max = false;
flot_max = 0;
flot = zeros(nb_noeud);

%% Algorithme
while flot_est_max == false
    % Recherche d'un chemin S-T
    [increment, pred] = bfs(depart, succ, capacite, flot);
    if  ~isKey(pred, fin)  % T n'est pas marque 
        flot_est_max = true;
    else
        % T est marque, on peut encore ameliorer le flot
        % Mise a jour de la matrice flot
        u = fin;
        while u ~= depart
            flot(pred(u), u) = flot(pred(u), u) + increment;
            u = pred(u);
        end
        % Augmentation du flot
        flot_max = flot_max + increment;
    end    
end


