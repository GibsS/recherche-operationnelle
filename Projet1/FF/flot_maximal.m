function flot = flot_maximal(vecteur_arrete, nb_noeud, depart, fin) 

%% Calcul du flot maximal d'un graphe
% Il s'agit d'une implémentation de l'algorithme de Ford et Fulkerson pour
% le calcul de flot maximal. On marque progressivement les sommets selon
% deux règles.

%% Données en entrée
% vecteur_arrete : il s'agit d'une matrice dont les lignes sont les arcs du
% graphe, avec, dans l'ordre, le sommet de départ, celui d'arrivée et la
% capacité maximale
% nb_noeud : le nombre de noeuds du graphe
% depart : le numéro du sommet de départ
% fin : le numéro du sommet d'arrivée

%% Données de sortie
% flot : vecteur contenant la capacité finale de chaque arrete, l'entrée i
% du flot correspond à l'arrete i du vecteur_arrete

%% Variables internes à l'algorithme
% nb_arrete : le nombre d'arretes
% marquage : vecteur retenant les sommets déja marqués, vecteur de booléen
% precedent : vecteur contenant, pour chaque sommet, un sommet ayant permis
% de le marquer si il est marqué, sinon 0 (on ne consulte de toute façon que les sommets marqués)
% aVisiter : liste de sommets qui ont été marqués mais dont on a pas encore
% regardé les sommets qui peuvent être marqués à partir de ceux-ci
% finTrouvee : booléen qui indique si on a trouvé un cycle allant du départ
% à l'arrivée de sommets marqués
% courant : le sommet marqué dont on étudie les sommets suivants et
% précédants pour savoir s'il sont marquables
% de 
% initialisation
nb_arrete = size(vecteur_arrete, 1);
precedent = zeros(nb_noeud,1);
flot = zeros(nb_arrete, 1);

fini = 0;

while fini == 0
    % marquage initial
    marquage = zeros(1,nb_noeud);
    precedent(depart) = -1;
    aVisiter = {};
    aVisiter{end + 1} = depart;
    finTrouvee = 0;

    % marquer de proche en proche tout les points
    while finTrouvee == 0 & size(aVisiter) > 0
        % récupérer courant
        courant = aVisiter{1};
        aVisiter = aVisiter(2:size(aVisiter));
             
        % récupérer voisin
        for i = 1:nb_arrete
            % si visitable directement
            if marquage(vecteur_arrete(i,1)) == 0
                % première rêgle de marquage :
                % courant est un sommet marqué, donc si l'arc part de
                % courant et n'est pas à sa capacité maximale, on marque le
                % sommet pointé (ie on l'ajoute à la liste des sommets à visiter)
                if (vecteur_arrete(i,1) == courant) & (vecteur_arrete(i,3) - flot(i) > 0)
                    aVisiter{end + 1} = vecteur_arrete(i,2);
                    precedent(vecteur_arrete(i,2)) = i;
                    % si on a trouvé un cycle, on s'arrete
                    if vecteur_arrete(i,2) == fin
                        finTrouvee = true;
                        break;
                    end
                end
                % deuxième rêgle de marquage :
                % le sommet courant est marqué, et si un arc pointe sur ce
                % sommet, dont la capacité n'est pas minimale, on marque le sommet de
                % départ de l'arc
                if (vecteur_arrete(i,2) == courant) & (flot(i) > 0 )
                    aVisiter{end + 1} = vecteur_arrete(i,1);
                    precedent(vecteur_arrete(i,1)) = i;
                    % si on a trouvé un cycle, on s'arrete
                    if vecteur_arrete(i,1) == fin
                        finTrouvee = true;
                        break;
                    end
                end
            end
        end
        marquage(courant) = 1;
    end
    if finTrouvee == 1
        % remonter les points pour obtenir le cycle et son poids maximal
        cycle = zeros(nb_arrete,1);
        noeud_courant = fin;
        % on prend un poids maximal initial suffisant
        poid = sum(vecteur_arrete(:,3)) + 1;
        while noeud_courant ~= depart
           arrete_courante = precedent(noeud_courant);
           % si le point a été marqué par la première règle
           if vecteur_arrete(arrete_courante,1) == noeud_courant
                cycle(arrete_courante) = -1;
                noeud_courant = vecteur_arrete(arrete_courante,2);
                poid = min(poid, flot(arrete_courante));
           % si le point a été marqué par la deuxième règle
           elseif vecteur_arrete(arrete_courante,2) == noeud_courant
                cycle(arrete_courante) = 1;
                noeud_courant = vecteur_arrete(arrete_courante,1);
                poid = min(poid, vecteur_arrete(arrete_courante,3) - flot(arrete_courante));
           end
        end
        % ajouter le cycle au flot
        flot = flot + poid*cycle;
    else 
       fini = 1; 
    end
end