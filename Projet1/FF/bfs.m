function [flot_res_min, pred] = bfs(depart, succ, capacite, flot)

%% Parcour en largeur du graphe (Breadth-first Search) pour trouver un chemin augmentant. 
% Le chemin trouve est le plus court possible. 

%% Donnees d'entree
% depart: source
% succ: containers.Map(int, list) qui contient les successeurs de chaque noeud, le
%       successeur de fin pris comme ?tant la liste vide.
% capacite: containers.Map(int, containers.Map(int,int)) qui contient les
%       capacites de chaque arrete du graphe.
% flot: matrice ou flot(u,v) indique le flot dans l'arrete u->v.

%% Donnees de sortie
% flot_res_min: flot residuel minimal sur le chemin trouve.
% pred: chemin augmentant. Contient les predecesseurs des noeuds contenus
%       dans le chemin. 

%% Initialisation
niveau = containers.Map({depart}, {0});
pred = containers.Map({depart}, {-1});
n = 1;
frontiere = depart; 
flot_res_min = Inf;

%% Algorithme
while ~isempty(frontiere)
    suivant = [];
    %suivant = zeros(1, nb_noeud);
    for i = 1:length(frontiere)
        u = frontiere(i);
        succ_u = succ(u);
        for j = 1:length(succ_u)
            v = succ_u(j);
            capacite_u = capacite(u);
            if ~isKey(niveau,v) && capacite_u(v) - flot(u,v) > 0
                niveau(v) = n;
                pred(v) = u;
                flot_res_min = min(flot_res_min, capacite_u(v) - flot(u,v));
                suivant(end+1) = v;
                %suivant(k) = v;
                %k = k+1;
            end    
        end    
    end
    frontiere = suivant;
    n = n+1;
end

end