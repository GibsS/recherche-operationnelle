%% Calcul d'une nouvelle frontiere en fonction des arcs sortants du cocycle
function [new_frontiere, cocycle_sortant] = marquage_sortant(cocycle, arcs, indicage_predecesseurs, frontiere)

n = size(indicage_predecesseurs,2);
m = size(arcs,2);
new_frontiere = [];
sol_int = zeros(1,m);
cocycle_sortant = cocycle;


for i=1:m
    if cocycle(i)
    	if arcs(2,i)==arcs(3,i)
    		sol_int(i) = 1;
            cocycle_sortant(i) = 0;
    	end
	end
end

i_deb = 1;
for j=1:n
    if sum(sol_int(i_deb:i_deb+indicage_predecesseurs(j)-1))>0
        new_frontiere = [new_frontiere j];
    end
    i_deb = i_deb+indicage_predecesseurs(j);
end

new_frontiere = sort(unique([frontiere new_frontiere]));