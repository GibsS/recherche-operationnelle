function [new_frontiere, cocycle_sortant] = marquage_sortant(cocycle, arcs, indicage_predecesseurs, frontiere, marques)

n = size(indicage_predecesseurs,2);
m = size(arcs,2);
new_frontiere = [];
sol_int = zeros(1,m);
cocycle_sortant = cocycle;


i_deb = 1;
for j=1:n
    bool = (indicage_predecesseurs(j)>0);
    for i=i_deb:i_deb+indicage_predecesseurs(j)-1
        bool = bool && cocycle(i) && marques(arcs(1,i)) && (arcs(2,i)>=arcs(3,i));
    end
    if bool
        new_frontiere = [new_frontiere j];
        cocycle_sortant(i_deb:i_deb+indicage_predecesseurs(j)-1)=0;
    end
    i_deb = i_deb+indicage_predecesseurs(j);
end

new_frontiere = sort(unique([frontiere new_frontiere]));