function [new_cocycle_entrant, new_cocycle_sortant] = maj_cocycles(cocycle_entrant, cocycle_sortant, arcs, indicage_predecesseurs, frontiere)

n = size(indicage_predecesseurs,2);
m = size(arcs,2);
new_cocycle_entrant = cocycle_entrant;

for i=1:m
    if cocycle_entrant(i)
        x = arcs(1,i);
        new_cocycle_entrant(i) = -(any(frontiere==x)-1);
    end
end

new_cocycle_sortant = cocycle_sortant;

i_deb = 1;
for j=1:n
    if (any(frontiere==j))
        new_cocycle_sortant(i_deb:i_deb+indicage_predecesseurs(j)-1) = 0;
    end
    i_deb = i_deb+indicage_predecesseurs(j);
end