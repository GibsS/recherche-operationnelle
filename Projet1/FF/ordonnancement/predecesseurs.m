function sol = predecesseurs(i, indicage_predecesseurs, arcs, marques)

m = size(arcs, 2);

i_init = 1;
for j=1:(i-1)
    i_init = i_init + indicage_predecesseurs(j);
end

sol_indices = [i_init:i_init+indicage_predecesseurs(i)-1];

sol = zeros(1,m);

sol(sol_indices) = 1;


for i=1:m
    if sol(i) && marques(arcs(1,i))
        sol(i) = 0;
    end
end