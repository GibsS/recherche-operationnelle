%indicage_predecesseurs = [1 3 2 1 1];
%arcs = [[2;-Inf;0;Inf] [3;-2;0;Inf] [4;-3;0;Inf] [5;-3;0;Inf] [4;-2;0;Inf] [5;-3;0;Inf] [1;0;0;Inf] [1;0;0;Inf]];

indicage_predecesseurs = [1 3 1 1 2];
arcs = [[2;-Inf;0;Inf] [3;-3;0;Inf] [4;-4;0;Inf] [5;-4;0;Inf] [1;-1;0;Inf] [1;-1;0;Inf] [3;-1;0;Inf] [4;-2;0;Inf]];


m = size(arcs, 2);
n = size(indicage_predecesseurs, 2);



global_condition = true;
while global_condition
    marques = zeros(1,n);
    marques(1) = 1;
    frontiere = [1];
    cocycle_entrant = zeros(1,m);
    cocycle_sortant = zeros(1,m);
    while length(frontiere)>0
        while length(frontiere)>0
            pred_it = predecesseurs(frontiere(1), indicage_predecesseurs, arcs, marques);
            cocycle_entrant = cocycle_entrant + pred_it;
            succ_it = successeurs(frontiere(1), indicage_predecesseurs, arcs, marques);
            cocycle_sortant = cocycle_sortant + succ_it;
            frontiere = frontiere(2:end);
        end
        cocycle_entrant = (cocycle_entrant>0);
        cocycle_sortant = (cocycle_sortant>0);
        frontiere = marquage_entrant(cocycle_entrant,arcs);
        [frontiere, cocycle_sortant] = marquage_sortant(cocycle_sortant,arcs,indicage_predecesseurs, frontiere, marques);
        [cocycle_entrant, cocycle_sortant] = maj_cocycles(cocycle_entrant, cocycle_sortant, arcs, indicage_predecesseurs, frontiere);
        marques(frontiere) = 1;
    end
    if marques(2)
        global_condition = false;
    else
        arcs_cocycle = find(cocycle_entrant>0);
        arcs_cocycle_sortant = find(cocycle_sortant>0);
        min_d = Inf;
        for i=1:size(arcs_cocycle,2)
            if arcs(4,arcs_cocycle(i))>arcs(3,arcs_cocycle(i))
                min_d = min(min_d,arcs(4,arcs_cocycle(i))-arcs(3,arcs_cocycle(i)));
            end
        end
        for i=1:size(arcs_cocycle_sortant,2)
            if arcs(3,arcs_cocycle_sortant(i))>arcs(2,arcs_cocycle_sortant(i))
                min_d = min(min_d,arcs(3,arcs_cocycle_sortant(i))-arcs(2,arcs_cocycle_sortant(i)));
            end
        end
        for i=1:size(arcs_cocycle,2)
            arcs(3,arcs_cocycle(i)) = arcs(3,arcs_cocycle(i)) + min_d;
        end  
        for i=1:size(arcs_cocycle_sortant,2)
            arcs(3,arcs_cocycle_sortant(i)) = arcs(3,arcs_cocycle_sortant(i)) - min_d;
        end   
    end
end

indice_courant = 2;
solution = [2];
while indice_courant>1
    for i=2:m
        
        i_init = 0;
        compteur = 0;
        while compteur<i
            i_init = i_init + 1;
            compteur = compteur + indicage_predecesseurs(i_init);
        end
                
        if arcs(1,i)==indice_courant && arcs(3,i)==arcs(4,i)
            indice_courant = i_init;
            solution = [solution i_init];
        end
        if (i_init==indice_courant) && arcs(2,i)==arcs(3,i)
            indice_courant = arcs(1,i);
            solution = [solution arcs(1,i)];
        end
        i=m+1;
    end
end

char(solution + 96)