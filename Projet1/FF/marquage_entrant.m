%% Calcul d'une nouvelle frontiere en fonction des arcs entrants du cocycle
function new_frontiere = marquage_entrant(cocycle, arcs)

m = size(arcs,2);
new_frontiere = [];

for i=1:m
    if cocycle(i)
    	if arcs(3,i)==arcs(4,i)
    		new_frontiere = [new_frontiere, arcs(1,i)];
    	end
	end
end

new_frontiere = sort(unique(new_frontiere));
