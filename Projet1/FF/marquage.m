function new_frontiere = marquage(cocycle, arcs)

m = size(arcs,2);
new_frontiere = [];

for i=1:m
    if cocycle(i)
    	if arcs(2,i)==arcs(3,i)
    		new_frontiere = [new_frontiere, arcs(1,i)];
    	end
	end
end

new_frontiere = sort(unique(new_frontiere));