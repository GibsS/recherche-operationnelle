%% Calcul des noeuds vers lequel un arc venant d'un noeud marqué pointe
function sol = successeurs(i, indicage_predecesseurs, arcs, marques)

n = size(indicage_predecesseurs,2);
m = size(arcs,2);
sol = zeros(1,m);

for j=1:m
    if arcs(1,j)==i
        sol(j) = 1;
    end
end

i_deb = 1;
for j=1:n
    if marques(j)
        sol(i_deb:i_deb+indicage_predecesseurs(j)-1) = 0;
    end
    i_deb = i_deb+indicage_predecesseurs(j);
end