#!/usr/bin/python

profs=[
"<div style='background-color:blue'><h2>Mme Droite</h2><h4>Maths</h4></div>",
"<div style='background-color:green'><h2>M Ellips</h2><h4>Maths</h4></div>",
"<div style='background-color:orange'><h2>Mme Proton</h2><h4>Physique</h4></div>",
"<div style='background-color:yellow'><h2>M Pascal</h2><h4>Info</h4></div>",
"<div style='background-color:cyan'><h2>Mme Ada</h2><h4>Info</h4></div>",
"<div style='background-color:red'><h2>M Young</h2><h4>Anglais</h4></div>",
"<div style='background-color:pink'><h2>Mme Gazelle</h2><h4>Sport</h4></div>",
"<div style='background-color:violet'><h2>M Biceps</h2><h4>Sport</h4></div>"
]


template = open("template.html","r")
edt = open("edt_affichage.html", "w")
matlab_ans = open("edt.html", "r")
ans = []

# Initialisation de ans, retour de Matlab
l=""
while not 'class="codeoutput"' in l:
	l = matlab_ans.readline()

while not 'x =' in l:
	l = matlab_ans.readline()

l = matlab_ans.readline()

for i in range(1,8*20*2 +1):
	l = matlab_ans.readline()
	ans.append(int(float(l.rstrip())))


# Ecriture de edt.html
for i in range(1,63 +1):
	edt.write(template.readline())


for i in range(1,5 +1):
	for p in range(1,2 +1):
		edt.write(template.readline())
		for j in range(1,4 +1):
			flag = False
			for k in range(1,8 +1):
				if ans[(p-1)*8*20 + (i-1)*8*4 + (j-1)*8 + k -1]:
					flag = True
					edt.write((template.readline()).replace("<div></div>",profs[k-1]))
			if not(flag):
				edt.write(template.readline())

for line in template:
	edt.write(line)



template.close()
edt.close()