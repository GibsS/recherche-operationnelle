%% Notre script pour calculer un emploi du temps respectant les contraintes
% Structure du script :
% 1. D�finition des constantes
% 2. D�finition en pseudo code des contraintes
% 3. Cr�ation des contraintes ligne par ligne (autant dans Aeq que A, b et
% beq)
% 4. Concat�nation des contraintes en Aeq, A, b et beq
% 5. Cr�ation de la fonction d'�valuation
% 6. R�solution 
% 7. Petit bout de code dont on a besoin pour produire l'emploi du temps en
% HTML

%% Variable du problème :
% x(prof, promo, creneau) 

% Constante du problème
% nombre de jour
nb_jour = 5;
% nombre de professeur
nb_prof = 8;
% nombre de promo
nb_promo = 2;
% nombre de creneau 
nb_creneau = 4*nb_jour;
% dimension du problème
dimension = nb_promo*nb_creneau*nb_prof;
% indice des professeur
idDroite = 1;
idEllips = 2;
idProton = 3;
idPascal = 4;
idDell = 5;
idYoung = 6;
idGazelle = 7;
idBigceps = 8;
% nombre de cours par prof et par promo
coursProfPromo = zeros(nb_prof,nb_promo); 
coursProfPromo(idDroite,1) = 5;
coursProfPromo(idEllips,2) = 4;
coursProfPromo(idProton,1) = 3;
coursProfPromo(idProton,2) = 3;
coursProfPromo(idPascal,1) = 6;
coursProfPromo(idDell, 2) = 6;
coursProfPromo(idYoung, 1) = 3;
coursProfPromo(idYoung, 2) = 3;
coursProfPromo(idGazelle, 1) = 1;
coursProfPromo(idBigceps, 2) = 1;

%% Contraintes intégré au problème linéaire:
% (1) : min somme(x(prof, promo, 4*jour + 1) + x(prof, promo, 4*(jour +1), prof = 1:nb_prof, promo = 1:nb_promo, jour = 1:nb_jour)
% (2) : les professeurs ont un nombre fixe de cours à donner : 
%   pour prof = 1:nb_prof, promo = 1:nb_promo : 
%   somme(x(prof, promo, creneau), creneau = 1..nb_creneau) = coursProfPromo(prof, promo)
% (3) : une promo n'a qu'un cours en même temps pour un créneau donné :
%   pour promo = 1:nb_promo, creneau = 1:nb_creneau :
%   somme(x(prof, promo, creneau), prof = 1:nb_prof)) <= 1
% (4) : un professeur n'a qu'un cours en même temps pour un créneau donné:
%   pour prof = 1:nb_prof, creneau = 1:nb_creneau :
%   somme(x(prof, promo, creneau), promo = 1:nb_promo)) <= 1
% (5) : une promo n'a qu'un seul au plus d'un certain cours par jour :
%   pour prof = 1:nb_prof, jour = 1:nb_jour, promo = 1:nb_promo :
%   somme(x(prof, promo, creneau), creneau = (jour - 1)*4 + 1:jour*4) <= 1
% (6) : Pas de cours le lundi matin (réservé au partiel) :
%   pour prof = 1:nb_prof, promo = 1:nb_promo, creneau = lundi() :
%   x(prof, promo, creneau) = 0;
% (7) : Mr Ellips n'a pas de cours le lundi matin :
%   pour promo = 1:nb_promo, pour creneau = lundi():lundi()+1 :
%   x(idEllips, promo, creneau) = 0
% (8) : Mme Proton n'a pas de cours le mercredi :
%   pour promo = 1:nb_promo, pour creneau = mercredi():mercredi()+3 :
%   x(idProton, promo, creneau) = 0
% (9) : Les cours de sport ont lieu le jeudi après midi :
%   x(idGazelle, 1, jeudi()+2) = 1;
%   x(idBigceps, 2, jeudi()+2) = 1;
% (10) : x(prof, promo, creneau = 0 ou 1

% (2) :
contrainte2A = zeros(nb_prof*nb_promo,dimension);
contrainte2b = zeros(nb_prof*nb_promo, 1);
for prof = 1:nb_prof
    for promo = 1:nb_promo
        for creneau = 1:nb_creneau 
            contrainte2A(prof + nb_prof*(promo-1), indice(prof,promo,creneau, nb_prof, nb_creneau)) = 1;
        end
        contrainte2b(prof + nb_prof*(promo-1)) = coursProfPromo(prof, promo);
    end
end
% (3) :
contrainte3A = zeros(nb_promo*nb_creneau,dimension);
contrainte3b = ones(nb_promo*nb_creneau, 1);
for promo = 1:nb_promo
    for creneau = 1:nb_creneau
        for prof = 1:nb_prof
            contrainte3A(promo + nb_promo*(creneau-1), indice(prof,promo,creneau, nb_prof, nb_creneau)) = 1;
        end
    end 
end

% (4) :
contrainte4A = zeros(nb_prof*nb_creneau,dimension);
contrainte4b = ones(nb_prof*nb_creneau, 1);
for prof = 1:nb_prof
    for creneau = 1:nb_creneau
       for promo = 1:nb_promo
           contrainte4A(prof + nb_prof*(creneau-1), indice(prof,promo,creneau, nb_prof, nb_creneau)) = 1;
       end
    end
end    

% (5) :
contrainte5A = zeros(nb_jour*nb_promo*nb_prof,dimension);
contrainte5b = ones(nb_jour*nb_promo*nb_prof,1);
for promo = 1:nb_promo
    for jour = 1:nb_jour
        for prof = 1:nb_prof
            for creneau =1:4
                contrainte5A(nb_jour*nb_promo*(prof-1) + nb_promo*(jour-1) + promo, indice(prof,promo,creneau + 4*(jour-1), nb_prof, nb_creneau)) = 1;
            end
        end
        for prof = 4:5
            contrainte5b(nb_jour*nb_promo*(prof-1) + nb_promo*(jour-1) + promo) = 2;
        end
    end
end    

% (6) :
contrainte6A = zeros(nb_promo*nb_prof,dimension);
contrainte6b = zeros(nb_promo*nb_prof, 1);
for prof = 1:nb_prof
    for promo = 1:nb_promo
        contrainte6A(prof+nb_prof*(promo-1), indice(prof,promo,1, nb_prof, nb_creneau)) = 1;
    end    
end

% (7) :
contrainte7A = zeros(nb_promo*2,dimension);
contrainte7b = zeros(nb_promo*2, 1);
for promo = 1:nb_promo
   contrainte7A(promo, indice(2,promo,1, nb_prof, nb_creneau)) = 1; 
   contrainte7A(promo + nb_promo, indice(2,promo,2, nb_prof, nb_creneau)) = 1;
end    

% (8) :
contrainte8A = zeros(nb_promo*4,dimension);
contrainte8b = zeros(nb_promo*4, 1);
for promo = 1:nb_promo
    for creneau = 1:4
        contrainte8A(promo + nb_promo*(creneau-1) , indice(3,promo,8 + creneau, nb_prof, nb_creneau)) = 1;
    end
end    

% (9) :
contrainte9A = zeros(nb_promo,dimension);
contrainte9b = ones(nb_promo,1);
for promo = 1:nb_promo
   contrainte9A(promo,  indice(6 + promo,promo,15, nb_prof, nb_creneau)) = 1;
end

Aeq = [contrainte2A;contrainte6A;contrainte7A;contrainte8A;contrainte9A];
beq = [contrainte2b;contrainte6b;contrainte7b;contrainte8b;contrainte9b];

A = [contrainte3A;contrainte4A;contrainte5A];
b = [contrainte3b;contrainte4b;contrainte5b];

%% D�finition de la fonction d'�valuation
% j'ai r�alis� une modification sur ce que j'ai pens� �tre une erreur : 
% avant ma modification, on avait f = ones(..), ce qui n'a pas de sens
f = zeros(nb_creneau*nb_promo*nb_prof,1);
for promo = 1:nb_promo
    for jour = 1:nb_jour
        for prof = 1:nb_prof
            f(indice(prof,promo,1 + 4*(jour-1), nb_prof, nb_creneau)) = 1;
            f(indice(prof,promo,4 + 4*(jour-1), nb_prof, nb_creneau)) = 1;
        end
    end
end    

%% R�solution
fitcon=1:320;
x = intlinprog(f,fitcon,A,b,Aeq,beq,zeros(320,1),ones(320,1));

%I=find(x==1);

%% Pr�sentation du r�sultat
x
%publish('edt.m')



