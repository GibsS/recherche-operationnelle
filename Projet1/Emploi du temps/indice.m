% Etant donn� que la solution se pr�sente sous la forme d'un vecteur, nous
% avons d�fini un bijection entre l'ensemble des cr�neaux et l'indice dans
% le vecteur solution
function x = indice(prof,promo,creneau,p,t)

x = prof+p*(creneau-1)+p*t*(promo-1);