% cette fonction est d�fini tel que indice_inverse(indice) = Identit�
function [prof,promo,creneau] = indice_inverse(indice,p,t)

prof=mod(indice-1,p)+1;
creneau=mod(floor((indice-1)/p),t)+1;
promo= floor(indice/(p*t))+1;